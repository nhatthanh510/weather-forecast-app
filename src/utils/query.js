export const parseObjectToQueryParam = (params) => {
  if ((Object.prototype.toString.call(params) === "[object Object]") !== true) {
    return "";
  }
  const qs = Object.keys(params)
    .map(
      (key) => `${encodeURIComponent(key)}=${encodeURIComponent(params[key])}`
    )
    .join("&");
  return qs;
};
