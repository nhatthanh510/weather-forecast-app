import { parseObjectToQueryParam } from "../../utils";

describe("parseObjectToQueryParam", () => {
  it("should parse correctly with encoding", () => {
    const mockData = {
      name: "Thanh Nguyen",
      age: 31,
    };
    const output = parseObjectToQueryParam(mockData);
    expect(output).toEqual("name=Thanh%20Nguyen&age=31");
  });

  it("should return empty string if params is not a object", () => {
    const output = parseObjectToQueryParam([1, 2, 3]);
    expect(output).toEqual("");
  });
});
