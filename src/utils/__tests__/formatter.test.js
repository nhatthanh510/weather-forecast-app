import { tempFormat } from "../../utils";

describe("tempFormat", () => {
  it("Round up correctly", () => {
    const output = tempFormat(29.591);
    expect(output).toEqual("30°");
  });
  it("Round down correctly", () => {
    const output = tempFormat(29.499);
    expect(output).toEqual("29°");
  });
});
