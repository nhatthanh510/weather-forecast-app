import { createRequest } from "../api";

const TEST_URL = "/api.coinmarketcap.com/crypto-list";

beforeEach(function () {
  global.fetch = jest.fn().mockImplementation(() =>
    Promise.resolve({
      ok: true,
      json: () => {
        return { message: "Success" };
      },
    })
  );
});

describe("createRequest", () => {
  it("Should return with correct response", async () => {
    const response = await createRequest(TEST_URL);
    expect(fetch).toHaveBeenCalledWith(TEST_URL, {});
    expect(response.message).toEqual("Success");
  });
});
