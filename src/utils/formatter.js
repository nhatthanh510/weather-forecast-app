export const tempFormat = (temp) => {
  return `${temp.toFixed(0)}°`;
};
