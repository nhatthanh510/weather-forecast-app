export const createRequest = (uri, options = {}) => {
  return new Promise((resolve, reject) => {
    window
      .fetch(uri, options)
      .then((response) => {
        if (!response.ok) {
          const error = {
            message: response.statusText,
            statusCode: response.status,
          };
          reject(error);
        }
        resolve(response.json());
      })
      .catch((error) => {
        reject(error);
      });
  });
};
