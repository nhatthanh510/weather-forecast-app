export const debounce = (fn, delay = 300) => {
  let timer
  let deferred;
  return (...args) => {
    if (deferred) {
      clearTimeout(timer)
    } else {
      deferred = defer()
    }

    timer = setTimeout(() => {
      clearTimeout(timer)
      const callbackPromise = fn(...args)
      Promise.resolve(callbackPromise).then(deferred.resolve)
      deferred = null
    }, delay)

    return deferred.promise
  }
}


const defer = () => {
  const deferred = {}
  deferred.promise = new Promise((resolve, reject) => {
    deferred.resolve = resolve
    deferred.reject = reject
  })
  return deferred
}