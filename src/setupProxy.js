const { createProxyMiddleware } = require("http-proxy-middleware");

module.exports = function (app) {
  app.use(
    "/forecast",
    createProxyMiddleware({
      target: "https://www.metaweather.com/api",
      changeOrigin: true,
    })
  );
};
