import React from "react";
import { shallow } from "enzyme";
import Spinner from "../Spinner";

describe("Spinner", () => {
  it("should match a snapshot", () => {
    expect(shallow(<Spinner isSpinning={false} />)).toMatchSnapshot();
  });
  it("should show spinner", () => {
    const wrapper = shallow(<Spinner isSpinning={true} />);
    expect(wrapper.find("div.spinner-border.text-primary")).toHaveLength(1);
  });
  it("should not show spinner", () => {
    const wrapper = shallow(<Spinner isSpinning={false} />);
    expect(wrapper.find("div.spinner-border.text-primary")).toHaveLength(0);
  });
});
