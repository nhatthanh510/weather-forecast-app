import { createRequest } from "../utils";
import { LOCATION_ENDPOINT } from "../config";

export const searchWeatherInfoByLocation = async (earthId) => {
  try {
    const result = await createRequest(`${LOCATION_ENDPOINT}/${earthId}/`);
    return result;
  } catch (error) {
    return {};
  }
};
