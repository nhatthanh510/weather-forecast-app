import { searchWeatherInfoByLocation } from "../weathers";
import { LOCATION_ENDPOINT } from "../../config";

const EARTH_ID = 1252431;
const TEST_URL = `${LOCATION_ENDPOINT}/${EARTH_ID}/`;

beforeEach(function () {
  global.fetch = jest.fn().mockImplementation(() =>
    Promise.resolve({
      ok: true,
      json: () => {
        return {
          message: "Test",
        };
      },
    })
  );
});

describe("searchWeatherInfoByLocation", () => {
  it("should return correct response", async () => {
    const response = await searchWeatherInfoByLocation(EARTH_ID);
    expect(fetch).toHaveBeenCalledWith(TEST_URL, {});
    expect(response.message).toBe("Test");
  });
});
