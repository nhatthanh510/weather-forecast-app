import { searchLocation } from "../locations";
import { LOCATION_ENDPOINT } from "../../config";

const TEST_URL = `${LOCATION_ENDPOINT}/search/?query=london`;

beforeEach(function () {
  global.fetch = jest.fn().mockImplementation(() =>
    Promise.resolve({
      ok: true,
      json: () => {
        return {
          message: "Test",
        };
      },
    })
  );
});

describe("searchLocation", () => {
  it("should fetch with correct endpoint", async () => {
    await searchLocation({
      query: "london",
    });
    expect(fetch).toHaveBeenCalledWith(TEST_URL, {});
  });

  it("should return correct response", async () => {
    const response = await searchLocation({
      query: "london",
    });
    expect(fetch).toHaveBeenCalledWith(TEST_URL, {});
    expect(response.message).toBe("Test");
  });
});
