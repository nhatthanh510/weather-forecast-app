import { createRequest, parseObjectToQueryParam } from "../utils";
import { LOCATION_ENDPOINT } from "../config";

export const searchLocation = async (requestParams) => {
  const qs = parseObjectToQueryParam(requestParams);
  try {
    const result = await createRequest(`${LOCATION_ENDPOINT}/search/?${qs}`);
    return result;
  } catch (error) {
    return [];
  }
};
