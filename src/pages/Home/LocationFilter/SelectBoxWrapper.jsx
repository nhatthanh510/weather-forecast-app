import React from "react";
import AsyncSelect from "react-select/async";

const SelectBoxWrapper = (props) => {
  return (
    <div className="react-select-wrapper">
      <AsyncSelect {...props} />
    </div>
  );
};

export default SelectBoxWrapper;
