import React from "react";
import { shallow } from "enzyme";
import LocationFilter from "../LocationFilter";
import SelectBoxWrapper from "../SelectBoxWrapper";

describe("LocationFilter", () => {
  const defaultProps = {
    setEarthId: () => {},
  };
  it("should match a snapshot", () => {
    expect(shallow(<LocationFilter {...defaultProps} />)).toMatchSnapshot();
  });
});

describe("SelectBoxWrapper", () => {
  const options = [
    { value: "option1", label: "Option One" },
    { value: "option2", label: "Option Two" },
  ];

  const defaultProps = {
    options: options,
    cacheOptions: true,
    classNamePrefix: "jest",
  };

  it("should match a snapshot", () => {
    expect(shallow(<SelectBoxWrapper {...defaultProps} />)).toMatchSnapshot();
  });

  it("should render select component", () => {
    const wrapper = shallow(<SelectBoxWrapper {...defaultProps} />);
    expect(wrapper.find("Async")).toHaveLength(1);
  });
});
