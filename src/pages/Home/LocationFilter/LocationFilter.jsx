import React, { useCallback } from "react";
import PropTypes from "prop-types";
import SelectBoxWrapper from "./SelectBoxWrapper";
import { searchLocation } from "../../../services/locations";
import { debounce } from '../../../utils/debouncePromise'

const LocationFilter = ({ setEarthId }) => {

  const asyncOptions =  useCallback(
    async (inputValue) => {
      const requestObj = {
        query: inputValue || "san",
      };
      const response = await searchLocation(requestObj);
  
      const formattedOptions = response.map((item) => {
        return {
          label: item.title,
          value: item.title,
          earthId: item.woeid,
        };
      });
      return formattedOptions;
    },
    [],
  )

  const debounceLoadOptions = debounce(asyncOptions, 500)

 
  const onChange = useCallback(
    async (options) => {
      setEarthId(options.earthId);
    },
    [setEarthId],
  )

  return (
    <div className="m-3">
      <SelectBoxWrapper
        placeholder="Search location"
        cacheOptions
        defaultOptions
        loadOptions={debounceLoadOptions}
        controlShouldRenderValue={false}
        onChange={onChange}
        maxMenuHeight={200}
      />
    </div>
  );
};

LocationFilter.propTypes = {
  setEarthId: PropTypes.func.isRequired,
};

export default LocationFilter;
