import React from "react";
import { shallow, mount } from "enzyme";
import Home from "../Home";

describe("Home", () => {
  it("should match a snapshot", () => {
    expect(shallow(<Home />)).toMatchSnapshot();
  });
});
