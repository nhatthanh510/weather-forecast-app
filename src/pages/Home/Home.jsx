import React, { useState, useEffect } from "react";
import DailyForecast from "../DailyForecast";
import LocationFilter from "./LocationFilter";
import { searchWeatherInfoByLocation } from "../../services/weathers";

const HCM_CITY_EARTH_ID = 1252431;

const Home = () => {
  const [earthId, setEarthId] = useState(HCM_CITY_EARTH_ID);
  const [isFetching, setIsFetching] = useState(false);
  const [weatherInfo, setWeatherInfo] = useState(null)

  const  { consolidated_weather: weatherList = [], title: locationTitle } =
  weatherInfo || {};

  useEffect(() => {
    const fetchWeatherInfo = async () => {
      setIsFetching(true);
      resetState();
      const weatherInfo = await searchWeatherInfoByLocation(earthId);
      setWeatherInfo(weatherInfo)
      setIsFetching(false);
    };

    fetchWeatherInfo();
  }, [earthId]);

  const resetState = () => {
    setWeatherInfo(null)
  };

  return (
    <div className="container-sm">
      <div className="h-100-px mw-400-px filter-box">
        <LocationFilter
          setEarthId={setEarthId}
        />
      </div>
      <div className="text-center">
        <DailyForecast
          weatherList={weatherList}
          locationTitle={locationTitle}
          isFetching={isFetching}
        />
      </div>
    </div>
  );
};

export default Home;
