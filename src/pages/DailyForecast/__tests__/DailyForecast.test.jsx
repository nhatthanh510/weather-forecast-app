import React from "react";
import { shallow } from "enzyme";
import DailyForecast from "../DailyForecast";
import { mockWeatherList } from "../__mocks__/weatherList";

const defaultProps = {
  weatherList: mockWeatherList,
  locationTitle: "San Diego",
  isFetching: true,
};

describe("DailyForecast", () => {
  it("should match a snapshot", () => {
    expect(shallow(<DailyForecast {...defaultProps} />)).toMatchSnapshot();
  });

  it("should render location title", () => {
    const wrapper = shallow(<DailyForecast {...defaultProps} />);
    expect(wrapper.find("h2.location-title").text().includes("San Diego")).toBe(
      true
    );
  });

  it("should not render weather list", () => {
    const wrapper = shallow(
      <DailyForecast {...defaultProps} weatherList={[]} />
    );
    expect(wrapper.find("div.day-forecast")).toHaveLength(0);
  });
});
