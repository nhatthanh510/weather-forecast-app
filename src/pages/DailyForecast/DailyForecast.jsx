import React from "react";
import PropTypes from "prop-types";
import dayjs from "dayjs";
import { tempFormat } from "../../utils/formatter";
import { STATIC_ASSET_URL } from "../../config/index.js";
import Spinner from "../../components/Spinner";

const DailyForecast = ({ weatherList, locationTitle, isFetching }) => {
  const renderCurrentDay = (item) => {
    const {
      id,
      applicable_date,
      min_temp,
      max_temp,
      weather_state_name,
      the_temp,
      weather_state_abbr,
    } = item;
    const weatherStateImgURL = `${STATIC_ASSET_URL}/img/weather/${weather_state_abbr}.svg`;
    return (
      <div key={id} className="row current-day mb-3">
        <div className="col-md-6">
          <div>
            <strong>{dayjs(applicable_date).format("dddd")}</strong> |{" "}
            {weather_state_name}
          </div>
          <div className="fs-1 fw-bold">
            {tempFormat(the_temp)}
            {weather_state_abbr && (
              <img
                className="img"
                width="24px"
                src={weatherStateImgURL}
                alt=""
              />
            )}
          </div>
        </div>
        <div className="col-md-6 temp-range">
          <div>
            <strong>Min: </strong>
            {tempFormat(min_temp)}
          </div>
          <div>
            <strong>Max: </strong>
            {tempFormat(max_temp)}
          </div>
        </div>
      </div>
    );
  };

  return (
    <div>
      <Spinner isSpinning={isFetching} />
      <h2 className="location-title">{locationTitle}</h2>
      {weatherList.map((item, index) => {
        const { id, applicable_date, min_temp, max_temp, weather_state_abbr } =
          item;
        const weatherStateImgURL = `${STATIC_ASSET_URL}/img/weather/${weather_state_abbr}.svg`;
        if (index === 0) {
          return renderCurrentDay(item);
        }
        return (
          <div
            className="row p-3 day-forecast"
            key={id}
          >
            <div className="col-md-3">
              {dayjs(applicable_date).format("dddd")}
            </div>
            <div className="col-md-3">
              {weather_state_abbr && (
                <img
                  className="img"
                  width="24px"
                  src={weatherStateImgURL}
                  alt=""
                />
              )}
            </div>
            <div className="col-md-3 text-center">
              <span className="text-muted">{tempFormat(min_temp)}</span>
            </div>
            <div className="col-md-3 text-center">
              <span>{tempFormat(max_temp)}</span>
            </div>
          </div>
        );
      })}
    </div>
  );
};

DailyForecast.propTypes = {
  weatherList: PropTypes.array,
  locationTitle: PropTypes.string,
  isFetching: PropTypes.bool,
};

export default DailyForecast;
