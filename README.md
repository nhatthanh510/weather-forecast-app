### Weather Forecast App

The application which can display a five day weather forecast based upon Metaweather API.  
Check out on `https://bitbucket.org/nhatthanh510/weather-forecast-app/src/master/`

### Prerequisites

You will need to have Node <= 12.x || >= 13.7 on your machine

### Install dependency

`yarn install`

## How to start

`yarn start`

## How to test

`yarn test`

Launches the test runner in the interactive watch mode.
